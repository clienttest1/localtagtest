#Need to install requests package for python
#easy_install requests
import requests
import json
import sys

# Mandatory tag key value as input from CI/CD team
mandatory_tags = {"apm_id": "12345", "costcenter": "abcd"}

# Set the request parameters
url = 'https://dev134570.service-now.com/api/now/table/incident?sysparm_limit=1'

# Eg. User name="admin", Password="admin" for this code sample.
user = 'Tunnav'
pwd = 'Superbest@22'

# Set proper headers
headers = {"Number":"INC0010002","Short description":"test","Content-Type":"application/json","Accept":"application/json"}

# Do the HTTP request
response = requests.get(url, auth=(user, pwd), headers=headers )

# Check for HTTP codes other than 200
if response.status_code != 200: 
    print('Status:', response.status_code, 'Headers:', response.headers, 'Error Response:',response.json())
    exit()

# Decode the JSON response into a dictionary and use the data
json.dumps("response.json")
data = response.json()
print(data["result"][0]["number"])

# Validate mandatory tags agaisnt SNOW CI
if data["result"][0]["number"] == mandatory_tags["apm_id"]:
    print("True")
else:
    print("False")





