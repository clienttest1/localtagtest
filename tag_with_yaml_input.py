#Need to install requests package for python
#easy_install requests
import requests
import json
import ruamel.yaml
import sys

def pass_url(app_name):

    my_urls = {
              "form_secure_site": "https://dev134570.service-now.com/api/now/table/incident?sysparm_limit=1",
              "ria_onboarding": "",
              "...":"..."
                 }
    
    return my_urls.get(app_name)
    
# Set the request parameters
url = pass_url("form_secure_site")

# Eg. User name="admin", Password="admin" for this code sample.
user = 'Tunnav'
pwd = 'Superbest@22'

# Set proper headers
headers = {"Number":"INC0010002","Short description":"test","Content-Type":"application/json","Accept":"application/json"}

# Do the HTTP request
response = requests.get(url, auth=(user, pwd), headers=headers )

# Check for HTTP codes other than 200
if response.status_code != 200: 
    print('Status:', response.status_code, 'Headers:', response.headers, 'Error Response:',response.json())
    exit()

# Decode the JSON response into a dictionary and use the data
json.dumps("response.json")
data = response.json()

#print(data["result"][0]["number"])

### Build a constructor to handle special character "!" in Yaml file
def general_constructor(loader, tag_suffix, node):
    return node.value
ruamel.yaml.SafeLoader.add_multi_constructor(u'!', general_constructor)

### Open Parameters yaml file template
with open (r"cfn/templates/parameters.yml", "r") as f:
    template_output = ruamel.yaml.safe_load(f)
    f.close()

### Validate SNOW CI tags agaisnt mandatory tags defined in parameters file
for item in data["result"]:
    if item.get("number") == template_output[0]["ParameterValue"]:
        print("True")
        with open ("result1.txt", "w") as f:
            f.writelines(template_output[0]["ParameterValue"])
            f.close()
    else:
        print("False")





